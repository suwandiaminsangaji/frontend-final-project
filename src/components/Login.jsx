import React, { useRef, useState } from "react";
import axios from "axios";
import {  FormGroup, Label, Input, Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import SecondHand from "./images/img-secondhand.jpeg";
import IconBack from "./icons/icon-back.jpeg";

export const LoginMenu = () => {
    const navigate = useNavigate();

  const emailField = useRef("");
  const passwordField = useRef("");

  const [errorResponse, setErrorResponse] = useState({
    isError: false,
    message: "",
  });

  const onLogin = async (e) => {
    e.preventDefault();

    try {
      const userToLoginPayload = {
        email: emailField.current.value,
        password: passwordField.current.value,
      };

      const loginRequest = await axios.post(
        "http://localhost:3200/api/user/login",
        userToLoginPayload
      );

      const loginResponse = loginRequest.data;

      if (loginResponse.success) {
        localStorage.setItem("token", loginResponse.data.token);
        navigate("/");
      }
    } catch (err) {
      console.log(err);
      const response = err.response.data;

      setErrorResponse({
        isError: true,
        message: response.message,
      });
    }
  };

  const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        // When the handler is invoked
        // inverse the boolean state of passwordShown
        setPasswordShown(!passwordShown);
    };

    
    return (
        <div>
            <div className="service mt-4" id="login">
                <div className="container">
                    <a className="back" href="/">
                    <img src={IconBack} alt="back"/>
                    </a>
                    <div className="row main">
                    <div className="col-lg-6 col-md-12 sm-remove">
                        <img src={SecondHand} alt="img-secondhand" className="img-secondhand"/>
                    </div>
                    <div className="col-lg-6 col-md-12 d-flex">
                        <form className="form-login" onSubmit={onLogin}>
                        <h1>Masuk</h1>
                        <div className="form-outline pt-1 mb-2">
                            <Label 
                            className="form-label" 
                            for="form2Example11">Email</Label> 
                            <input 
                            ref={emailField}  
                            className="form-control"
                            placeholder="Contoh: johndee@gmail.com" />
                        </div>
                        <div className="form-outline mb-3">
                            <Label 
                            className="form-label" 
                            for="form2Example22">Password</Label>
                            <input 
                            ref={passwordField}
                            type={passwordShown ? "text" : "password"}
                            id="password" 
                            className="form-control" 
                            placeholder="Masukkan password"/> 
                                    
                        </div>
                        <div className="text-center pt-1 mb-2">
                            <Button 
                            className="btn btn-dark btn-block fa-lg mb-3" 
                            type="submit">Masuk</Button>
                        </div>
                        <div className="d-flex align-items-center justify-content-center mb-text-L">
                            <p className="mb-0 me-2">Belum punya akun?</p>
                            <a className="text-muted" href="register">Daftar di sini</a>
                        </div>
                        </form>
                    </div>        
                    </div>
                </div>
            </div>
        </div>
    )

}
export default LoginMenu;
