import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
} from 'reactstrap';
import { Button } from 'reactstrap';
import { FiSearch } from 'react-icons/fi';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Link } from 'react-router-dom';
function CardProduct({
  products,
  filteredProduct,
  setFilteredProduct,
  filterResult,
}) {
  var settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    className: 'slider variable-width',
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div className="container">
      <h5 className="fw-bold mt-5">Telusuri Kategori</h5>
      <Slider {...settings} className="carousel-category">
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            style={{ width: 150 }}
            onClick={() => setFilteredProduct(products)}
          >
            <FiSearch /> Semua
          </Button>{' '}
        </div>
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            onClick={() => filterResult(1)}
          >
            <FiSearch /> Hobi
          </Button>{' '}
        </div>
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            onClick={() => filterResult(2)}
          >
            <FiSearch /> Kendaraan
          </Button>{' '}
        </div>
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            onClick={() => filterResult(3)}
          >
            <FiSearch /> Baju
          </Button>{' '}
        </div>
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            onClick={() => filterResult(4)}
          >
            <FiSearch /> Elektronik
          </Button>{' '}
        </div>
        <div className="item-kategori m-3 ">
          <Button
            className="btn-kategori border-radius"
            onClick={() => filterResult(5)}
          >
            <FiSearch /> Kesehatan
          </Button>{' '}
        </div>
      </Slider>
      <div className="card-row">
        {filteredProduct.map((product) => (
          <Link to={`/product/details/${product.id}`}>
            <Card key={product.id} className="card-item">
              <CardImg
                alt="image produk jual"
                src={product.image}
                top
                width="100%"
              />
              <CardBody>
                <CardTitle tag="h5">{product.name}</CardTitle>
                <CardSubtitle className="mb-2 text-muted" tag="h6">
                  {product.description},{product.quantity}
                </CardSubtitle>
                <CardText>Rp {product.price}</CardText>
              </CardBody>
            </Card>
          </Link>
        ))}
      </div>
    </div>
  );
}
export default CardProduct;
