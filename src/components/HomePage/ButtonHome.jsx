import { Button } from 'reactstrap';
import { MdAdd } from 'react-icons/md';
import { Link } from 'react-router-dom';

function ButtonHome() {
  return (
    <div className="position-fixed bottom-5 start-50 translate-middle-x">
      <Link to="/daftarjual">
        <Button className="button-jual shadow-lg  border-radius">
          <MdAdd /> Jual
        </Button>
      </Link>
    </div>
  );
}

export default ButtonHome;
