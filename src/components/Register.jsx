import axios from "axios";
import React, { useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import { FiEye } from "react-icons/fi";
import SecondHand from "./images/img-secondhand.jpeg";
import IconBack from "./icons/icon-back.jpeg";



export const RegisterMenu = () => {
    const navigate = useNavigate();

    const nameField = useRef("");
    const emailField = useRef("");
    const passwordField = useRef("");

    const onRegister = async (e) => {
        e.preventDefault();

        try {
            const userToRegisterPayload = {
                name: nameField.current.value,
                email: emailField.current.value,
                password: passwordField.current.value,
            };

            const registerRequest = await axios.post(
                "http://localhost:3200/api/user/register",
                userToRegisterPayload
            );

            const registerResponse = registerRequest.data;

            if (registerResponse.success) navigate("/login");
        } catch (err) {
            console.log(err);
        }
    };

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        // When the handler is invoked
        // inverse the boolean state of passwordShown
        setPasswordShown(!passwordShown);
    };
    return (
        <div>
            <div className="mt-4" id="register">
                <div className="container">
                    <a className="back" href="/"><img src={IconBack} alt="back"/></a>
                    <div className="row main" inline onSubmit={onRegister}>
                        <div className="col-lg-6 col-md-12 sm-remove">
                            <img src={SecondHand} alt="img-secondhand" className="img-secondhand"/>
                        </div>
                        <div className="col-lg-6 col-md-12 d-flex">
                            <Form className="form-login">
                                <h1>Daftar</h1>
                                <div className="form-outline mb-2">
                                    <Label className="form-label" for="form2Example11">Nama</Label> 
                                    <input 
                                    ref={nameField}
                                    type="text" 
                                    id="nama" 
                                    className="form-control"
                                    placeholder="Nama Lengkap" />
                                </div>
                                <div className="form-outline mb-2">
                                    <Label className="form-label" for="form2Example11">Email</Label> 
                                    <input 
                                    ref={emailField}
                                    type="email" 
                                    id="email" 
                                    className="form-control"
                                    placeholder="Contoh: johndee@gmail.com" />
                                </div>
                                <div className="form-outline mb-3">
                                    <Label className="form-label" for="form2Example22">Password</Label>
                                    <input 
                                    ref={passwordField}
                                    type={passwordShown ? "text" : "password"}
                                    id="password" 
                                    className="form-control" 
                                    placeholder="Masukkan password"/>        
                                </div>
                                <div className="text-center pt-1 mb-2">
                                    <Button className="btn btn-dark btn-block fa-lg mb-3" type="submit">Daftar</Button>
                                </div>
                                <div className="d-flex align-items-center justify-content-center pb-4 mb-text-R">
                                    <p className="mb-0 me-2">Sudah punya akun?</p>
                                    <a className="text-muted" href="login">Masuk di sini</a>
                                </div>
                            </Form>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    )

}
export default RegisterMenu;