import { useState } from 'react'
import axios from 'axios'
import { useNavigate } from "react-router-dom";

const Form = ({ infoProfil }) => {
    const [form, setForm] = useState({})
    const navigate = useNavigate();

    return (
        <div className='form'>
            <div className="container mb-2 mt-4">
             <div className="row">
                <div className="col">
                <a onClick={() => navigate("/daftarjual")}>
                <img className='left-arrow' style={{marginRight: 0}} src="left-arrow.png" alt />
                </a>
                </div>
                <div className="col">
                <input className='picture-camera' id='img' name='image' for='image' type="file" alt="Submit" onChange={(e) => setForm({
                    ...form,
                    image: e.target.files[0]
                })} />
                <label for="img"><img className='picture-input' src='picture.png'></img></label>
                </div>
             </div>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Nama*</label>
                <input placeholder='Nama' type="text" className="form-control mt-2" onChange={(e) => setForm({
                    ...form,
                    name: e.target.value
                })}/>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Kota*</label>
                <select required className="form-select mt-2" id="inputGroupSelect01" onChange={(e) => setForm({
                    ...form,
                    city: e.target.value
                })}>
                    <option value="" disabled selected hidden>Pilih Kota</option>
                    <option value={1}>Semarang</option>
                    <option value={2}>Surabaya</option>
                    <option value={3}>Malang</option>
                </select>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Alamat*</label>
                <input placeholder='Contoh: Jalan Ikan Hiu 33' type="text" className="form-control mt-2" onChange={(e) => setForm({
                    ...form,
                    address: e.target.value
                })}/>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">No Handphone*</label>
                <input placeholder='contoh: +628123456789' type="text" className="form-control mt-2" onChange={(e) => setForm({
                    ...form,
                    phone: e.target.value
                })}/>
            </div>
            <br />
            <button type="submit" className="btn btn-simpan button-purple" onClick={() => infoProfil(form)}>Simpan</button>
        </div>
    )
}

export default Form