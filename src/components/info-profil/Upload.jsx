import React from 'react';

const Upload = ({img, ...rest}) => {
    return (
        <div className="col upload">
            <label for="img">{img && <img for="img" className='preview' src={img} alt="preview" />}</label>
            <input id="img" className='picture-camera' type="file" {...rest}/>
        </div>
    )
}

export default Upload