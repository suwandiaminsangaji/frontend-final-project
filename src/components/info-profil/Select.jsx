import React from 'react';

const Select = ({label, ...rest}) => {
    return (
        <div className="form-group mb-2">
        <p className='label'>{label}</p>
        <select required className="form-select mt-2" {...rest}>
            <option value="" disabled selected hidden>Pilih Kota</option>
            <option value={1}>Semarang</option>
            <option value={2}>Surabaya</option>
            <option value={3}>Malang</option>
            <option value={4}>Banda Aceh</option>
            <option value={5}>Bandung</option>
            <option value={6}>Jakarta</option>
        </select>
    </div>
    )
}

export default Select