import { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import { useNavigate, useParams } from "react-router-dom";
import Input from "./Input"
import Select from "./Select";
import Button from "./Button";
import Upload from "./Upload";

const UpdateInfoProfil = () => {
    const navigate = useNavigate();
    
    const [image, setImage] = useState(null)
    const [name, setName] = useState(null)
    const [city, setCity] = useState(null)
    const [address, setAddress] = useState(null)
    const [phone, setPhone] = useState(null)
    const [imagePreview, setImagePreview] = useState('picture.png');


    const updateInfoProfil = async (e) => {
        e.preventDefault();

        console.log('image: ', image)
        console.log('name: ', name)
        console.log('city: ', city)
        console.log('address: ', address)
        console.log('phone: ', phone)

        let formField = new FormData()
        formField.append('name',name)
        formField.append('city',city)
        formField.append('address',address)
        formField.append('phone',phone)

        if(image !== null) {
          formField.append('image', image)
        }

        const token = localStorage.getItem("token");

        await axios({
          method: 'post',
          url:'http://localhost:3200/api/user/details',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: formField
        }).then(response=>{
          console.log(response.data);
          navigate('/')
          alert("Successfully Adding Info Profil !");
        })
    }

    const onImageUpload = (e) => {
        const file = e.target.files[0];
        setImage(file)
        setImagePreview(URL.createObjectURL(file))
    }

    return (
        <div className=''>
            <nav className="navbar navbar-expand-lg nav-info-profil">
            <div class="container-fluid">
                <a style={{fontFamily: 'Poppins'}} className="navbar-brand navbar-info-profil" href="#">
                 <img className="rectangle-navbar" style={{marginRight: 285}} src="rectangle.png" width={150} height={24} alt />
                 Lengkapi Info Akun
                </a>
            </div>
            </nav>
            <div className='form'>
            <div className="container mb-2 mt-4">
             <div className="row">
                <div className="col">
                <a onClick={() => navigate("/daftarjual")}>
                <img className='left-arrow' style={{marginRight: 0}} src="left-arrow.png" alt />
                </a>
                </div>
                <Upload required onChange={(e) => onImageUpload(e)} img={imagePreview}></Upload>
             </div>
            </div>
            <Input label="Nama*" name="name" value={name} onChange={(e) => setName(e.target.value)} placeholder='Nama'></Input>
            <Select label="Kota*" name="city" value={city} onChange={(e) => setCity(e.target.value)}></Select>
            <Input label="Alamat*" name="address" value={address} onChange={(e) => setAddress(e.target.value)} placeholder='Contoh: Jalan Ikan Hiu 33'></Input>
            <Input label="No Handphone*" name="phone" value={phone} onChange={(e) => setPhone(e.target.value)} placeholder='contoh: +628123456789'></Input>
            <br />
            <Button title="Simpan" onClick={updateInfoProfil}></Button>
            </div>
        </div>
    )
}

export default UpdateInfoProfil