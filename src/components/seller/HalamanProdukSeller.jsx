import NavbarProduct from './NavbarProduct';
import { UncontrolledCarousel } from 'reactstrap';
import "./StyleBuyer.css";


function CardPenjual() {
    return (
        <div className='nama-penjual'>
            <div>
            <img src="img/user.png" alt=""/>
            </div>
            <div className='identitas'>
            <p className='f-14'><strong>Nama Penjual</strong></p>
            <p className='kota f-12'>kota</p>
            </div>
        </div>
    );
  }

function popup(){
    return (
        <div id="popup">
        <div class="window">
            <a href="/" class="close-button" title="Close">X</a>
            <h5>Masukkan Harga Tawarmu</h5>
            <p>Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.</p>
            <h5>Harga Tawar</h5>
            <input class="form-control me-2 mr-4" type="tawarHarga" placeholder="Rp 0,00"></input>
        </div>
    </div>
    );
}

function NameProduk() {
    return (
        
            <div class="card card-konten">
                 <div class="card-body">                  
                    <h5 className="card-title" id="NamaProduk">Jam Tangan Casio</h5>
                    <h6 className="card-subtitle mb-2" id="kategori">Acsesories</h6>
                    <p className="card-text" id="HargaProduk">Rp 250.000</p>
                    <a href="#"><button class="btn btn-dark btn-block fa-lg mb-2 pt-2 pb-2" type="button">Terbitkan</button></a>
                    <a href="#"><button class="btn-preview" type="button">Edit</button></a>
                 </div>
             </div>
    );
}

function ImageProduk() {
    return (
        <div className="container mb-4 pd">
            <div className="container-left">
                <UncontrolledCarousel
                className="carousel-product"
                items={[
                    {
                    key: 1,
                    src: 'https://picsum.photos/id/200/1200/600',                  
                    },
                    {
                    key: 2,
                    src: 'https://picsum.photos/id/400/1200/600',
                    },
                    {
                    key: 3,
                    src: 'https://picsum.photos/id/600/1200/600',
                    },
                ]}
                />
            </div>
       </div>
    );
}

function Produk() {
    return (
        <div class="card card-konten">
            <div class="card-body">
                <h5>Deskripsi</h5>
                <p class="" id="descripsi">Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                qui officia deserunt mollit anim id est laborum.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
    );
}
const HalamanProdukSeller = () => {
    return (
        <div class=" ">
            <NavbarProduct/>
            <div className='container'>
                <div className='konten-wrapper'>                            
                <div className='konten'>
                    <div className='row'>
                        <div className='col-md-8'>
                            <ImageProduk/>
                            <Produk/> 
                        </div>
                        <div className='col-md-4'>
                            <NameProduk/>
                            <CardPenjual/>
                        </div>                                                              
                    </div>
                </div>
                </div>
            </div>
        </div>
    );
};

export default HalamanProdukSeller;