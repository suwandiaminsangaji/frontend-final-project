import React from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import arrow from '../../img/arrow.png';
import ImgProduk from '../images/img-produk.jpg';

const InputProduk = () => (
    <div>
        <div className='profil-section'>
            <div class="container">
                <div className='menu-profil'>
                    <Row>
                        <Col sm="4">
                            <button className='profil-button'></button>
                            <div className='button-back2'>
                                <img src={arrow} alt="" />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>

        <div className='container'>
            <div className='button-back'>
                <a href="/"><img src={arrow} alt="" /></a>
            </div>
            <div className='content-profil'>
                <div className='form-profil'>
                    <Form className='f-12'>
                        <FormGroup>
                            <Label for="exampleEmail">
                            Nama Produk
                            </Label>
                            <Input
                            id="NamaProduk"
                            placeholder="Nama Produk"
                            type="text"
                            className="input-profil f-14"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleText">
                            Harga Produk
                            </Label>
                            <Input
                            id="HargaProduk"
                            type="text"
                            placeholder="Rp 0,00"
                            className="input-profil f-14"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleSelect">
                            Kategori                            
                            </Label>
                            <Input
                            id="Kategori"
                            name="select"
                            placeholder="Pilih Kategori"
                            type="select"
                            className="input-profil f-14">
                            <option>
                                Pilih Kategori
                            </option>
                            <option>
                                Pakaian
                            </option>
                            <option>
                                Makanan
                            </option>
                            <option>
                                Elektronik
                            </option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleText">
                            Deskripsi
                            </Label>
                            <Input
                            id="deskripsi"
                            name="text"
                            type="textarea"
                            placeholder="Contoh: Jalan Ikan Hiu 33"
                            className="input-profil f-14"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">
                            Foto Produk
                            </Label>
                            <div>
                                <label><img src={ImgProduk} alt='' />
                                <input type="file" accept=".jpg,.jpeg,.png" style={{ display: 'none' }} /></label>
                            </div>
                        </FormGroup>
                        <div className="row">
                            <div className="col">
                                <div className="mb-3">
                                    <button type="submit" class="btn btn-preview">Preview</button>
                                </div>
                            </div>
                            <div className="col">
                                <div className="mb-3">
                                    <button type="submit" className="btn btn-terbit" >Terbitkan</button>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    </div>
)

export default InputProduk;