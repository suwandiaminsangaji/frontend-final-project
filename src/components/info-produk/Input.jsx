import React from 'react';

const Input = ({label, ...rest}) => {
    return (
        <div className='form-group mb-2'>
            <p className="label">{label}</p>
            <input className="form-control mt-2" {...rest} />
        </div>
    )
}

export default Input