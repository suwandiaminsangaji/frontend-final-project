import React from 'react';

const ButtonPreview = ({title, ...rest}) => {
    return (
        <button className="btn btn-preview" {...rest}>{title}</button>
    )
}

export default ButtonPreview