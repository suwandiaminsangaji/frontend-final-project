import { useState } from 'react'
import axios from 'axios'
import ProductPageSeller from '../Seller/ProductPage'
import { useNavigate } from "react-router-dom";

const Form = ({ infoProduk }) => {
    const [form, setForm] = useState({})
    const navigate = useNavigate();
    return (
        <div className='form'>
            <div className="container mb-2">
             <div className="row">
                <div className="col">
                <a href="/">
                <img className='left-arrow-1' src="left-arrow.png" alt />
                </a>
                </div>
             </div>
            </div>
            <div className='form-seller'>
            <div className="form-group mb-2">
                <label htmlFor="">Nama Produk</label>
                <input placeholder='Nama Produk' type="text" className="form-control mt-2" onChange={(e) => setForm({
                    ...form,
                    name: e.target.value
                })}/>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Harga Produk</label>
                <input placeholder='Rp 0,00' type="text" className="form-control mt-2" onChange={(e) => setForm({
                    ...form,
                    price: e.target.value
                })}/>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Kategori</label>
                <select required className="form-select mt-2" id="inputGroupSelect01" onChange={(e) => setForm({
                    ...form,
                    category_id: e.target.value
                })}>
                    <option value="" disabled selected hidden>Pilih Kategori</option>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                </select>
            </div>
            <div className="form-group mb-2">
                <label htmlFor="">Deskripsi</label>
                <input placeholder='Contoh: Jalan Ikan Hiu 33' type="text" className="form-control description mt-2" onChange={(e) => setForm({
                    ...form,
                    description: e.target.value
                })}/>
            </div>
            <div className="form-group mb-2">
            <label htmlFor="">Foto Produk</label>
            <br />
            <input className='input-image mt-2' id="img-pic" name='image' for='image' type="file" alt="Submit" onChange={(e) => setForm({
                    ...form,
                    image: e.target.files[0]
                })}/>
            <label for="img-pic"><img className='input-image-produk' src="input-image.png"></img></label>
            </div>
            </div>
            <br />
            <div className="row">
                <div className="col">
                    <div className="mb-3">
                        <button type="button" className="btn btn-preview" onClick={() => navigate("/seller-product")}>Preview</button>
                    </div>
                </div>
                <div className="col">
                    <div className="mb-3">
                        <button type="submit" className="btn btn-terbit" onClick={() => navigate("/daftarjual")}>Terbitakan</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Form