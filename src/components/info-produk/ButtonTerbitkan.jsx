import React from 'react';

const ButtonTerbitkan = ({title, ...rest}) => {
    return (
        <button className="btn btn-terbit" {...rest}>{title}</button>
    )
}

export default ButtonTerbitkan