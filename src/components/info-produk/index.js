import { useState, useEffect } from 'react'
import axios from 'axios'
import { useNavigate, useParams } from "react-router-dom";
import Input from './Input';
import Select from './Select';
import ButtonPreview from './ButtonPreview';
import ButtonTerbitkan from './ButtonTerbitkan';
import Upload from './Upload';

const InfoProduct = () => {
    const navigate = useNavigate();
    const { id } = useParams();
    
    const [name, setName] = useState(null)
    const [price, setPrice] = useState(null)
    const [categoryId, setCategoryId] = useState(null)
    const [description, setDescription] = useState(null)
    const [image, setImage] = useState(null)
    const [imagePreview, setImagePreview] = useState('input-image.png');


    useEffect(() => {
      loadInfoProduct();
    }, [])


    // load info produk by its is and show data to forms by value

    let loadInfoProduct = async () => {
    const result = await axios.get('http://localhost:3200/api/product/update');
    console.log(result.data.name);

    setName(result.data.name);
    setPrice(result.data.price);
    setCategoryId(result.data.categoryId);
    setDescription(result.data.description);
    setImage(result.data.image);
    }

    // Update s single info product

    const AddInfoProduct = async () => {

        console.log('name: ', name)
        console.log('price: ', price)
        console.log('category: ', categoryId)
        console.log('description: ', description)
        console.log('image: ', image)

        let formField = new FormData()
        formField.append('name',name)
        formField.append('price',price)
        formField.append('categoryId',categoryId)
        formField.append('description',description)

        if(image !== null) {
            formField.append('image', image)
          }

        const token = localStorage.getItem("token");

        await axios({
          method: 'put',
          url:'http://localhost:3200/api/product/update',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: formField
        }).then(response=>{
          console.log(response.data);
          navigate('/daftarjual')
          alert("Successfully Adding Info Product !");
        })
    }

    

    const onImageUpload = (e) => {
        const file = e.target.files[0];
        setImage(file)
        setImagePreview(URL.createObjectURL(file))
    }

    
    return (
        <div className=''>
            <nav className="navbar navbar-expand-lg nav-info-produk">
            <div class="container-fluid">
                <a style={{fontFamily: 'Poppins'}} className="navbar-brand" href="#">
                 <img className="rectangle-navbar" style={{marginRight: 335}} src="rectangle.png" width={150} height={24} alt />
                </a>
            </div>
            </nav>
            <div className='form'>
            <div className="container mb-2">
             <div className="row">
                <div className="col">
                <a onClick={() => navigate("/")}>
                <img className='left-arrow-1' src="left-arrow.png" alt />
                </a>
                </div>
             </div>
            </div>
            <div className='form-seller'>
            <Input label="Nama Produk" name="name" value={name} onChange={(e) => setName(e.target.value)} placeholder='Nama Produk'></Input>
            <Input label="Harga Produk" name="price" value={price} onChange={(e) => setPrice(e.target.value)} placeholder='Rp 0,00'></Input>
            <Select label="Kategori" name="categoryId" value={categoryId} onChange={(e) => setCategoryId(e.target.value)}></Select>
            <Input label="Deskripsi" name="description" value={description} onChange={(e) => setDescription(e.target.value)} placeholder='Contoh: Jalan Ikan Hiu 33' style={{height: 80, marginBottom: 9}}></Input>
            <Upload label="Foto Produk" onChange={(e) => onImageUpload(e)} img={imagePreview}></Upload>
            </div>
            <br />
            <div className="row">
              <div className="col">
                  <div className="mb-3">
                    <ButtonPreview title="Preview" onClick={() => navigate("/seller-product/:id")}></ButtonPreview>
                  </div>
              </div>
              <div className="col">
                  <div className="mb-3">
                      <ButtonTerbitkan title="Terbitakan" onClick={AddInfoProduct}></ButtonTerbitkan>
                  </div>
              </div>
            </div>
        </div>
        </div>
    )
}

export default InfoProduct