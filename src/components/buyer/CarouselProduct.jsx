import { UncontrolledCarousel } from 'reactstrap';
import CardProduct from './CardProduct';
function CarouselProduct({ detailProduct, detailUser }) {
  console.log(detailUser);
  return (
    <div className="container container-carousel-parent">
      <div className="d-flex container-carousel">
        <UncontrolledCarousel
          className="carousel-seller-product"
          items={[
            {
              key: 1,
              src: '',
            },
            {
              key: 2,
              src: '',
            },
            {
              key: 3,
              src: '',
            },
          ]}
        />
        <div className="card-right ">
          <CardProduct detailProduct={detailProduct} detailUser={detailUser} />
        </div>
      </div>
      <div className="card card-deskripsi border-radius">
        <div className="card-body">
          <h5 className="card-title">Deskripsi</h5>
          <p className="card-text">{detailProduct.description}</p>
        </div>
      </div>
    </div>
  );
}

export default CarouselProduct;
