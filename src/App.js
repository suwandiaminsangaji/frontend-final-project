import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from 'react-router-dom';
import LoginMenu from './components/Login';
import RegisterMenu from './components/Register';
import DaftarJualMenu from './components/DaftarJual';
import InfoProfil from './components/info-profil';
import InfoProduk from './components/info-produk';
import HomePage from './components/HomePage';
import ProductPageSeller from './components/Seller/HalamanProdukSeller';
import ProductPageBuyer from './components/Buyer';
import AkunSaya from './components/AkunSaya';
import InfoPenawar from './components/info-penawar'
import InfoStatus from './components/info-status'
import './App.css';

function App() {
  return (
    <div className="App">
       <Routes>
          <Route path="/" element={<HomePage/>} />
          <Route path='/login' element={<LoginMenu/>} />
          <Route path='/register' element={<RegisterMenu/>} />
          <Route path='/daftarjual' element={<DaftarJualMenu/>} />
          <Route path="/info-profil" element={<InfoProfil />} />
          <Route path="/info-produk" element={<InfoProduk />} />
          <Route path="/seller-product" element={<ProductPageSeller />} />
          <Route path="/product/details/:id" element={<ProductPageBuyer />} />
          <Route path="/akun" element={<AkunSaya/>} />
          <Route path="/info-penawaran" element={<InfoPenawar/>} />
          <Route path="/info-status" element={<InfoStatus/>} />
      </Routes>
    </div>
  );
}

export default App;
